# Cài đặt môi trường phát triển PHP7 sử dụng Docker
## Môi trường

- PHP: php:7.2.7-fpm-alpine3.7
- Webserver: httpd:2.4.33-alpine
- MySQL: mysql:5.6.40

## Cài đặt

> Note: Máy tính đã cài đặt môi trường docker và docker-compose

Clone repo: https://gitlab.com/quang.liem.dang.99/env_php7.git

```sh
cd env_php7
docker-composer up
```

## Cấu trúc thư mục:



| Plugin | README |
| ------ | ------ |
| apache | chứa Dockerfile và apache.conf của webserver apache |
| mysql_data | thư mục lưu trữ dữ liệu sinh ra bởi container MySQL |
| php | chứa Dockerfile của php |
| public_html | thư mục chứa source code |
| docker-compose.yml | file cấu hình chạy docker-compose |


## Cách sử dụng

### Webserver
- Port run apache trên host: 8080 (Truy cập http://localhost:8080/ để khởi chạy ứng dụng trên web.)
- Port run apache trên container: 80

### Database
**Cách 1: phpmyadmin**
- Port run phpmyadmin trên host: 8081
- Port run phpmyadmin trên container: 80
- Môi trường ánh xạ tới: mysql (cổng 3306)
- Giao diện hoạt động ở url: http://localhost:8081/
- Tài khoản: root
- Mật khẩu: rootpassword

**Cách 2: Sử dụng công cụ khác (workbench,...)**
- Chạy workbench, connect tới host 127.0.0.1 và port 3306
- Tài khoản: root
- Mật khẩu: rootpassword

## php

Dockerfile:
```sh
FROM php:7.2.7-fpm-alpine3.7        #Phiên bản php đang sử dụng.
RUN apk update; \
    apk upgrade;
RUN docker-php-ext-install mysqli   #Cài đặt mysqli để thao tác với hệ quản trị CSDL MySQL.
```

## public_html

Chứa source code của project.

## docker-compose.yml

```sh
version: "3.2"
services:
  php:
    build: './php/'
    networks:
      - backend
    volumes:
      - ./public_html/:/var/www/html/       #Ánh xạ file source code vào trong container.
  apache:
    build: './apache/'
    depends_on:
      - php
      - mysql
    networks:
      - frontend
      - backend
    ports:
      - "8080:80"                           #Ánh xạ cổng apache đang sử dụng của máy local với container đang chạy.
    volumes:
      - ./public_html/:/var/www/html/       #Ánh xạ file source code vào trong container.
  mysql:
    image: mysql:5.6.40
    networks:
      - backend
    ports:
      - "3306:3306"                         #Ánh xạ cổng của mysql đang sử dụng của máy local với container.
    environment:
      - MYSQL_ROOT_PASSWORD=rootpassword    #Thiết lập mật khẩu cho tài khoản root truy cập mySQL.
    volumes:
      - ./mysql_data/:/var/lib/mysql/ 
  phpmyadmin:
    image: phpmyadmin/phpmyadmin
    ports:
        - 8081:80                           #Ánh xạ cổng chạy phpmyadmin đang sử dụng của máy local với container.
    environment:
      PMA_HOST: mysql                       #Thiết lập tên server của mySQL cho phpmyadmin.
      PMA_PORT: 3306                        #Thiết lập cổng của mySQL cho phpmyadmin.
    links:
     - mysql
    networks:
      - backend
networks:
  frontend:
  backend:

```



